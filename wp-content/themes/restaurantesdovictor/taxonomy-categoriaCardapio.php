<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Restaurantes_do_Victor
 */
	$categoriaAtual = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$frase 			= $configuracao['opt-frase-cardapio'];

	session_start();
	$_SESSION["nomeFranquia"]= 'Bar do Victor';
get_header(); ?>
	<div class="pg pg-cardapio">

		<!-- BANNER -->
		<section class="banner" style="background:url(<?php bloginfo('template_directory'); ?>/img/bg-cardapio.png)">
			<p>Cardápio</p>
			<span><?php echo $frase  ?></span>
		</section>

		<div class="container">
			<section class="areaCardapio">
				
				<div class="row">
					<!-- SIDEBAR -->
					<div class="col-md-3">
						
						<!-- MENU  -->
						<nav class="sidebarCardapio">
							<?php

								// DEFINE A TAXONOMIA
								$taxonomia = 'categoriaCardapio';

								// LISTA AS CATEGORIAS PAI DOS SABORES
								$categoriasCardapio = get_terms( $taxonomia, array(
									'orderby'    => 'count',
									'hide_empty' => 0,
									'parent'	 => 0
								));
								
								
								
								foreach ($categoriasCardapio as $categoriaCardapio):								
										$nome = $categoriaCardapio->name;	
										$descricao = $categoriaCardapio->description;
										$categoriaAtivaImg = z_taxonomy_image_url($categoriaCardapio->term_id);
										if (isset($categoriaAtivaImg) && !empty($categoriaAtivaImg)) {
											$categoriaAtivaImg = $categoriaAtivaImg;
										}else{
											$categoriaAtivaImg = get_bloginfo('template_directory')."/img/dinner.png";
										}
										if ($nome == $categoriaAtual->name): 											
										
							?>
								<a href="<?php echo get_category_link($categoriaCardapio->term_id); ?>" class="ativoLink">
									<img src="<?php echo $categoriaAtivaImg ?>" alt="" class="img-respnsive">
									<h2><?php echo $nome  ?></h2>
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</a>
							<?php else: ?>
								<a href="<?php echo get_category_link($categoriaCardapio->term_id); ?>">
									<img src="<?php echo $categoriaAtivaImg ?>" alt="" class="img-respnsive">
									<h2><?php echo $nome  ?></h2>
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</a>
							<?php  endif;endforeach;?>
						</nav>
					
					</div>

					<div class="col-md-9" id="prato">
						<?php
						$taxonomy = get_taxonomy( $taxonomy );
						$taxonomy_name = $taxonomy->name;
						$term_id = get_queried_object()->term_id;
						$custom_field = get_field( 'layout', $taxonomy_name . '_' . $term_id );
						?>

						<!-- PRATO -->
						<?php 

							// VERIFICAÇÃO LAYOUT SE FOR BEBIDAS OU PRATO NORMAL
							if ($custom_field != "layout2"):
				           
				            	// LOOP DE PRATOS
								if ( have_posts() ) : while( have_posts() ) : the_post();
					            $fotocardapio = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					            $fotocardapio = $fotocardapio[0];
					            $Subtitulo = rwmb_meta('Restaurantesdovictor_prato_Subtitulo');	
					            $galeriaFotos = rwmb_meta('Restaurantesdovictor_prato_galeria');					           
						?>
							<!-- PRATOS  -->
							<div class="cardapioPratos" id="prato">
								<div class="row">
									<div class="col-sm-9">
										<h2>■ <?php echo  get_the_title() ?></h2>
										<?php if($Subtitulo != ''): ?><span><?php echo  $Subtitulo ?></span><?php endif; ?>
										<p><?php echo get_the_content() ?></p>
									</div>
									<div class="col-sm-3">									
										<?php if($fotocardapio != "http://restaurantesvictor.com.br/wp-content/uploads/2016/09/asa.png" || $fotocardapio != ""): ?>
										<a href="<?php echo  $fotocardapio ?>" data-lightbox-title="<?php echo  get_the_title() ?>" data-lightbox-gallery="gallery" >
											<div class="cardapioFotoPrato" style="background:url(<?php echo  $fotocardapio ?>)"></div>
										</a>
										<?php endif; ?>
									</div>
								</div>
							</div>							

							<?php  endwhile;endif; wp_reset_query(); ?>

						<?php else: ?>

							<!-- BEBIDAS -->
							<div class="cardapioBebidas" id="prato">
								<ul>

									<?php	
										//LOOP DE BEBIDAS 
										if ( have_posts() ) : while( have_posts() ) : the_post();
										$fotocardapio = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$fotocardapio = $fotocardapio[0];
										$Subtitulo = rwmb_meta('Restaurantesdovictor_prato_Subtitulo');
										$bebidas = rwmb_meta('Restaurantesdovictor_prato_detalhe');	
									?>
										<li>
											<h2><?php echo get_the_title() ?></h2>
											<?php
											if ($bebidas != 0 || $bebidas != ""):
												
											
											 foreach ($bebidas as $bebidas): 
											 	$bebiba = $bebidas ;

												$bebiba = explode("|", $bebiba);
										 	?>
											<h3><?php echo $bebiba[0]  ?></h3>
											<p><?php echo $bebiba[1] ?></p>
											<?php endforeach; endif; ?>
											
										</li>
									<?php  endwhile;endif; wp_reset_query(); ?>

								</ul>

							</div>
						<?php endif; ?>


					</div>
				</div>

				<div class="pratodestaque" style="background:url(<?php echo $configuracao['opt-cardapioSL']['url']; ?> )"></div>

			</section>	
		</div>	
		
</div>
<script>
// $(window).load(function(){
// $(window).scrollTop(0);
// });
	
// 	window.onload = function () {
		
// 		$('html, body').animate({
// 			scrollTop: $('#prato').offset().top - 120
// 		}, 1000);
// 	}
</script>

<?php
get_footer();
 include (TEMPLATEPATH . '/inc/scriptMapa.php');
