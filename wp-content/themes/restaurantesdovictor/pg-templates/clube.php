<?php

/**
 * Template Name:Clube
 * Description: Página inicial do site  Restaurantes do Victor
 *
 * @package Restaurantes_do_Victor
 */

$formulario  = rwmb_meta('Restaurantesdovictor_clube_formulario'); 
$fotoClube = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoClube = $fotoClube[0];
// LOOP DA FOTO 
if ( have_posts() ) : while( have_posts() ) : the_post();
$descricao = get_the_content();
 endwhile; endif;  
get_header(); ?>
<!-- Modal -->
<div class="modal fade" id="a" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
       <div class="modal-body">
      	    <button type="button" class="close" data-dismiss="modal">&times;</button>
  		<div class="pg pg-contato">
			
			<p class="subtitulo-contato">FORMULÁRIO DE CONTATO CLUBE</p>							

			<div class="formulario-contato">

				<?php echo do_shortcode($formulario); ?>

			</div>
		
		</div>

      </div>
      
    </div>
  </div>
</div>

<div class="pg pg-clube">

	<div class="banner-evento" style="background:url(<?php echo $fotoClube ?>)">
		<div class="nome-evento">
			<h3>Faça Parte do clube</h3>
			<p><?php //echo $descricao  ?></p>
		</div>
	</div>

	<section class="area-clube">
		<h6 class="hidden">Club Victor</h6>
		<ul>
			<?php 
				// LOOP DE POST CLUBE
				$clube = new WP_Query( array( 'post_type' => 'clube', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
				while ( $clube->have_posts() ) : $clube->the_post();
					$fotoClubepost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoClubepost = $fotoClubepost[0];

			 ?>
			<li>  
				<div class="clube-foto" style="background:url(<?php echo $fotoClubepost ?>)">
					<div class="lente">
						<span><?php echo get_the_title() ?></span>
						<p><?php echo get_the_content() ?></p>							
					</div>
				</div>					
			</li>
			<?php endwhile;wp_reset_query(); ?>
			
		</ul>
		
		<a href="http://clubedovictor.giver.com.br/"  target="_blank" class="quero-participar hvr-shutter-in-vertical" >Quero participar</a>
	</section>	
</div>

<?php get_footer();  include (TEMPLATEPATH . '/inc/scriptMapa.php');?>