<?php

/**
 * Template Name: São Lourenço
 * Description: Página inicial do site  Restaurantes do Victor
 *
 * @package Restaurantes_do_Victor
 */



$quemsomsoFoto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

$quemsomsoFoto = $quemsomsoFoto[0];


$quemSomos_historia   =	$configuracao['pier_historia']; 

$quemSomos_comoSurgiu = $configuracao['pier_como_surgiu']; 





get_header(); ?>


	<div class="pg pg-quem-somos" style="display:">
		
		<?php 
			// VERIFICANDO CHECKBOX VÍDEO OU FOTO
			if ($checkbox_quemsomos  == 1):			
			
		?>

		<div class="videoGeral">
			<video  preload="auto" autoplay="" loop="" muted="" src="<?php echo $quemSomos_video_banner ?>"></video>
		</div>

		<?php else: ?>

		<div class="bg-quem-somos"  style="display:;background:url(<?php echo $configuracao['s_banner']['url'] ?>)"></div>
		
		<?php endif; ?>

		<!-- ÁREA QUEM SOMOS DESCRIÇÃO -->
		<section class="container area-quemsomos">
			<div class="row">
				<div class="col-md-6">
					<div class="texto-quem-somos">
						<h2><?php echo $configuracao['s_nome']; ?> </h2>
						<span><?php echo $configuracao['s_endereco'];  ?> <i class="fa fa-map-marker" aria-hidden="true"></i></span>
						<p><?php echo get_the_content() ?> </p>
					</div>
				</div>

				<div class="col-md-6 text-center">
					<div class="area-historia-como-surgiu">					
						<ul>
							<li>
								<p>
								<?php echo $quemSomos_historia;  ?>
								</p>
							</li>
							<li class="direito">
								<p>
								<?php echo $quemSomos_comoSurgiu; ?>
								</p>
							</li>
						</ul>
					</div>					
				</div>
			</div>
		</section>

		<!-- LINHA DO TEMPO -->
		<section class=" area-linhado-tempo">
				<h6 class="hidden">Linha do Tempo</h6>
			
				<div class="linhaTempo">
				</div>
				
				<ul>
					<?php 
						$x = 0;
						$linhadotempo = new WP_Query( array( 'post_type' => 'linhadotempo', 'orderby' => 'ASC', 'posts_per_page' => -1 ) );
	   					 while ( $linhadotempo->have_posts() ) : $linhadotempo->the_post();
						$ano = rwmb_meta('Restaurantesdovictor_LinhadotempoAno'); 
						$descricao = rwmb_meta('Restaurantesdovictor_LinhadotempoDescricao'); 

						// IF PARA FAZER MUDANÇA NO LAYOUT
						if($x % 2 == 0):
					 ?>

					<li class="linhaBaixo" title="<?php echo $ano ?>">
						<div class="textoLinhaPar" title="<?php echo $ano ?>">
							<span><?php echo get_the_title() ?></span>
							<p><?php echo $descricao ?></p>
							
						</div>
						
					</li>

					<?php else: ?>
					
					<li class="linhaCima" title="<?php echo $ano ?>">
						<div class="textoLinhaPar" title="<?php echo $ano ?>">
							<span><?php echo get_the_title() ?></span>
							<p><?php echo $descricao ?></p>
							
						</div>
						
					</li>

					<?php endif; $x++;endwhile;wp_reset_query(); ?>
				</ul>
			

		</section>
	</div>	

<?php get_footer(); 
 include (TEMPLATEPATH . '/inc/scriptMapa.php');
?>