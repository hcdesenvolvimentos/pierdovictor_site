<?php

session_start();

$_SESSION['nomeFranquia'] = '';

/**
 * Template Name: Inicial
 * Description: Página inicial do site  Restaurantes do Victor
 *
 * @package Restaurantes_do_Victor
 */

get_header(); ?>
	<title><?php echo get_the_title(); ?> </title>

	<!-- Modal -->
	<?php 
		$cont = 0;
		// LOOP DE ESTABELECIMENTO
		$modais = new WP_Query( array( 'post_type' => 'estabelecimento', 'orderby' => 'rand', 'posts_per_page' => -1 ) );
	    while ( $modais->have_posts() ) : $modais->the_post();

			$formularioReserva = rwmb_meta('Restaurantesdovictor_form_reserva'); 
			
			
	?>	
	<div class="modal fade" id="<?php echo $cont ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      
	       <div class="modal-body">
	      	    <button type="button" class="close" data-dismiss="modal">&times;</button>
	  		<div class="pg pg-contato">
				
				<p class="subtitulo-contato text-center">SOLICITAR RESERVA</p>							

				<div class="formulario-contato">

					<?php //echo do_shortcode($formularioReserva);
						echo $formularioReserva;
						
				 	?>

				</div>
			
			</div>

	      </div>
	      
	    </div>
	  </div>
	</div>
	<?php $cont++; endwhile; wp_reset_query(); ?>

	<!-- Modal -->
	<?php 
		$i = 0;
		// LOOP DE ESTABELECIMENTO
		$modais = new WP_Query( array( 'post_type' => 'estabelecimento', 'orderby' => 'rand', 'posts_per_page' => -1 ) );
	    while ( $modais->have_posts() ) : $modais->the_post();

			$formularioReserva = rwmb_meta('Restaurantesdovictor_form_reserva'); 
			$horarioFuncionamento = rwmb_meta('Restaurantesdovictor_horarioFuncionamento_estabelecimentoA'); 
			$horarioFuncionamentoJ = rwmb_meta('Restaurantesdovictor_horarioFuncionamento_estabelecimentoJ'); 
			
	?>	
	<div class="modal  fade" id="horario<?php echo $i ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      
	       <div class="modal-body modalMobile">
	      	    <button type="button" class="close" data-dismiss="modal">&times;</button>
	  		<div class="pg pg-contato">
				
				<div class="formulario-contato">
					
					<div class="modoalHorarios">
						<p class="subtitulo-contato text-center">Horários de atendimento</p>	
						<span><?php echo get_the_title() ?> - <?php echo $local = rwmb_meta('Restaurantesdovictor_local_estabelecimento');  ?></span>

						<b>Almoço</b>
						<?php foreach ($horarioFuncionamento  as $horarioFuncionamento): ?>
							<small><?php  echo $horarioFuncionamento ; ?></small>
						<?php endforeach; ?>
						<b>Jantar</b>
						<?php foreach ($horarioFuncionamentoJ  as $horarioFuncionamentoJ): ?>
							<small><?php  echo $horarioFuncionamentoJ ; ?></small>
						<?php endforeach; ?>
					</div>

				</div>
			
			</div>

	      </div>
	      
	    </div>
	  </div>
	</div>
	<?php $i++; endwhile; wp_reset_query(); ?>

	<div class="pg pg-inicial-franquias" id="topoDesktop">
			
		<nav class="navMenu">
			<ul class="menuDrop">
				<li><a href="#">Reservas <i class="fa fa-angle-down" aria-hidden="true"></i></a>
					<ul>
						<?php 
							$cont = 0;
							// LOOP DE ESTABELECIMENTO
							$estabelecimento = new WP_Query( array( 'post_type' => 'estabelecimento',  'posts_per_page' => -1 ) );
				            while ( $estabelecimento->have_posts() ) : $estabelecimento->the_post();
				            $formularioReserva = rwmb_meta('Restaurantesdovictor_form_reserva'); 
				            $local = rwmb_meta('Restaurantesdovictor_local_estabelecimento');
							 if ($formularioReserva != ""):
						?>	
						<li><a  href="#myModal" data-toggle="modal" class="facaSuareserva" data-target="#<?php echo $cont ?>"><?php echo get_the_title() ?> <br> <?php echo  $local  ?></a></li>
						<?php endif; $cont++; endwhile; wp_reset_query(); ?>    
						<li><a target="_black" href="http://restaurantesvictor.com.br/contato-petiscaria-do-victor/">Petiscaria do Victor <br> Santa Felicidade</a></li>               
					</ul>
				</li>
				<li><a href="#">Delivery <i class="fa fa-angle-down" aria-hidden="true"></i></a>
					<ul>
						<li><a target="_black" href="https://www.ifood.com.br/bardovictor_saolourenco?Origem=SiteParceiro&produto=intermediario">Restaurantes Victor <br> São Lourenço</a></li>
						<li><a target="_black" href="https://www.ifood.com.br/bardovictorpra%C3%A7adaespanha?Origem=SiteParceiro&produto=intermediario">Restaurantes Victor <br> Praça da Espanha</a></li>
						<li><a target="_black" href="https://www.spoonrocket.com.br/bistrodovictor">Restaurantes Victor <br> ParkShoppingBarigüi</a></li>                    
					</ul>
				</li>
				<li><a target="_black" href="http://conteudo.restaurantesvictor.com.br/proposta-espaco-para-evento">Eventos </a></li>
				<li><a href="#">Contato <i class="fa fa-angle-down" aria-hidden="true"></i></a>
					<ul>
						<li><a target="_black" href="http://restaurantesvictor.com.br/contato-bar-do-victor/  ">Restaurantes Victor <br> São Lourenço</a></li>
						<li><a target="_black" href="http://restaurantesvictor.com.br/contato-bar-do-victor-praca-da-espanha/ ">Restaurantes Victor <br> Praça da Espanha</a></li>
						<li><a target="_black" href="http://restaurantesvictor.com.br/contato-bistro-do-victor/ ">Restaurantes Victor <br> ParkShoppingBarigüi</a></li>
						<li><a target="_black" href="http://restaurantesvictor.com.br/contato-petiscaria-do-victor/ ">Petiscaria do Victor <br> Santa Felicidade</a></li>                    
					</ul>
				</li>
				<li><a href="#">Quem Somos <i class="fa fa-angle-down" aria-hidden="true"></i></a>
					<ul class="ultimoItem">
						<?php 
							$i = 0;
							// LOOP DE ESTABELECIMENTO
							$estabelecimento = new WP_Query( array( 'post_type' => 'estabelecimento', 'orderby' => 'rand', 'posts_per_page' => -1 ) );
				            while ( $estabelecimento->have_posts() ) : $estabelecimento->the_post();
				            $link = rwmb_meta('Restaurantesdovictor_link_estabelecimento');
				             $local = rwmb_meta('Restaurantesdovictor_local_estabelecimento');
							
							if ($link != ""):
						?>	
						<li><a  href="<?php echo $link ?>" class="linkBanner" target="_blank" ><?php echo get_the_title() ?> <br> <?php echo  $local  ?></a></li>
						<?php endif; $i++; endwhile; wp_reset_query(); ?>                 
					</ul>
				</li>
				            
			</ul>
		</nav>

			<!-- CARROSSEL DE DESTAQUE  -->
			<section class="carrosselDestaqueFranquias">
				<button id="btnCarrossenDestaqueF" class="btnPaginaInicial btnCarrossenDestaqueF"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
				<button id="btnCarrossenDestaqueT" class="btnPaginaInicialRight btnCarrossenDestaqueT"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
				<h6 class="hidden"> Restaurantes Victor  </h6>
			
					<div id="carrossel-destaque-inicial" class="owl-Carousel">
					<?php 
					$i = 0;
						// LOOP DE DESTAQUE
						$destaqueInicial = new WP_Query( array( 'post_type' => 'destaque-inicial', 'posts_per_page' => -1 ) );
			            while ( $destaqueInicial->have_posts() ) : $destaqueInicial->the_post();
			            // FOTO DESTACADA
						$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoDestaque = $fotoDestaque[0];

						$check = rwmb_meta('Restaurantesdovictor_checkbox_destaqueinicial'); 
						$urlVideo = rwmb_meta('Restaurantesdovictor_video_destaqueinicial'); 
						$arte = rwmb_meta('Restaurantesdovictor_checkbox_imagem'); 
						$foto_ativar = rwmb_meta('Restaurantesdovictor_foto_ativar');
						
						if ($foto_ativar != 1) {
					
						
					?>
					<div class="item" style="background:url(<?php echo $fotoDestaque ?>)">
						<?php if ($arte != "1") : ?>	
						<video loop autoplay muted class="background" style="background:url(<?php echo $fotoDestaque ?>)" id="video">
							<source src="<?php echo $urlVideo ?>" type="video/mp4">
						</video>

						<!-- LOGO -->
						<img src="<?php bloginfo('template_directory'); ?>/img/logoPierBranca.png" class="logo" alt="Logo Restaurantes Victor">

						<!-- DESCRIÇÃO -->
						<div class="textoDescricaoCasa">
							<p><?php echo get_the_title() ?></p>
							<span><?php echo get_the_content() ?> </span>

							<a href="#test1" id="" class="linkCarrosselDestaque linkBanner">Conheça nossas embarcações</a>
						</div>
						<?php else: ?>
						<a href="<?php echo $urlVideo  ?>" class="linkBannerTopoTarget" target="_blank" ></a>
						<?php endif; ?>

						<!-- LOGO DAS EMBARCAÇÕES -->
						<div class="logoEmbarcacoesFranquias" style="display: none;">
							<ul>
								<?php 

									// LOOP DE ESTABELECIMENTO
									$estabelecimento = new WP_Query( array( 'post_type' => 'estabelecimento', 'orderby' => 'rand', 'posts_per_page' => -1 ) );
						            while ( $estabelecimento->have_posts() ) : $estabelecimento->the_post();

										// FOTO DESTACADA
										$fotoestabelecimento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$fotoestabelecimento = $fotoestabelecimento[0];	
										// METABOX DE CONTÚDO
										$logo = rwmb_meta('Restaurantesdovictor_logo_estabelecimento'); 
										$url = rwmb_meta('Restaurantesdovictor_linkpagina_estabelecimento'); 
										
								?>	
								<li>
									<?php foreach ($logo as $logo):?>
									<a href="<?php echo $url ?>" target="_blank">
										<img src="<?php echo $logo['full_url'] ?>" class="img-responsive logos" alt="<?php echo get_the_title() ?>">
									</a>
									<?php endforeach; ?>
								</li>
								<?php endwhile; wp_reset_query(); ?>
								 
							</ul>
						</div>	


					</div>
					<?php };$i++; endwhile; wp_reset_query(); ?>
				</div>
			
			</section>
			
			<!-- EMBARCAÇÕES -->
			<div class="containerFull">
				<section class="embarcacoesFranquias" id="test1">
					<ul>

					<?php 
						$i = 0;
						// LOOP DE ESTABELECIMENTO
						$estabelecimento = new WP_Query( array( 'post_type' => 'estabelecimento',  'posts_per_page' => -1 ) );
			            while ( $estabelecimento->have_posts() ) : $estabelecimento->the_post();

							// FOTO DESTACADA
							$fotoestabelecimento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoestabelecimento = $fotoestabelecimento[0];	
							// METABOX DE CONTÚDO
							$logo = rwmb_meta('Restaurantesdovictor_logo_estabelecimento'); 
							$link = rwmb_meta('Restaurantesdovictor_link_estabelecimento'); 
							$url = rwmb_meta('Restaurantesdovictor_linkpagina_estabelecimento'); 
							$local = rwmb_meta('Restaurantesdovictor_local_estabelecimento'); 
							$endereco = rwmb_meta('Restaurantesdovictor_endereco_estabelecimento'); 
							$telefone = rwmb_meta('Restaurantesdovictor_telefone_estabelecimento'); 
							$horarioFuncionamento = rwmb_meta('Restaurantesdovictor_horarioFuncionamento_estabelecimentoA'); 
							$horarioFuncionamentoJ = rwmb_meta('Restaurantesdovictor_horarioFuncionamento_estabelecimentoJ'); 
							$facebook = rwmb_meta('Restaurantesdovictor_facebookestabelecimento'); 
							$twitter = rwmb_meta('Restaurantesdovictor_twitter_estabelecimento'); 
							$formularioReserva = rwmb_meta('Restaurantesdovictor_form_reserva'); 
							
					?>		
						<!-- EMBARCAÇÃO -->
						<li id="<?php echo  $i  ?>embarcacao">
							
							<div class="infoTopo">
								<div class="row">
									
									<!-- LOGO -->
									<div class="col-xs-4 areaLogoNome">
										<?php foreach ($logo as $logo):?>
										<a href="<?php echo $url ?>" target="_blank" class="img-responsive">
											<img src="<?php echo $logo['full_url'] ?>" class="img-responsive" alt="<?php echo get_the_title() ?>">
										</a>
										<?php endforeach; ?>
									</div>

									<div class="col-xs-8 areaLogoNome">
										<!-- NOME DAS EMBARCAÇÕES -->
										<a href="<?php echo $url ?>" target="_blank">
											<h2 itemprop="name"><?php echo get_the_title() ?></h2>
											<span itemprop="locality"><?php echo $local ?></span>
										</a>

										<!-- REDES SOCIAIS -->
										<div class="redessociais">
											<a href="<?php echo $facebook ?>" target="_blank"><i class="fa hvr-pop fa-facebook" aria-hidden="true"></i></a>
											<a href="<?php echo $twitter ?>" target="_blank"><i class="fa hvr-pop fa-instagram" aria-hidden="true"></i></a>
										</div>

									</div>

								</div>
							</div>

							<!-- FOTO EMBARCAÇÕES -->
							<div class="fotoEmbarcacaoFranquias" style="background:url(<?php echo $fotoestabelecimento ?>)">
								<div class="lente"></div>
							</div>

							<!-- ÁREA CONTATO -->
							<div class="informacoesdeContatoFranquias">
								<div class="enderecos">
									<a href="https://www.google.com.br/maps/place/<?php echo $endereco  ?>,Curitiba-PR" target="_blank">
										<i class="fa fa-home" aria-hidden="true"></i>
										<p itemprop="street-address"><?php echo $endereco  ?></p>
										<span>Ver mapa</span>
									</a>
								</div>
								<div class="enderecos">
									<a href="tel:<?php echo $telefone ?>">
										<i class="fa fa-phone" aria-hidden="true"></i>
										<p itemprop="tel"><?php echo $telefone ?></p>
									</a>
								</div>
								
								<div class="enderecos" id="horarioFuncionamento">
									<a href="">
										<i class="fa fa-clock-o" aria-hidden="true"></i>
										<p class="horarios">Horários</p>
										<div class="horariosFuncionamento">
										<strong>Almoço:</strong>
										<?php  
											foreach ($horarioFuncionamento as $horarioFuncionamento):
												$horaario = $horarioFuncionamento
										?>
											
											<p><?php echo $horaario ?></p>
										<?php endforeach; ?>

										<strong>Jantar:</strong>
										<?php  
											foreach ($horarioFuncionamentoJ as $horarioFuncionamentoJ):
												$horaarioJ = $horarioFuncionamentoJ
										?>
											
											<p><?php echo $horaarioJ ?></p>
										<?php endforeach; ?>

										</div>
									</a>
								</div>

							</div>

							<div class="btnsReservaLinkSite">
								<?php if ($formularioReserva != ""):?>
								<a href="#myModal" data-toggle="modal" class="facaSuareserva" data-target="#<?php echo $i ?>">Faça sua reserva</a>
								<?php endif ?>
								<?php if ($link != ''):?>
								<a href="<?php echo $url  ?>">Quem Somos <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
								<?php endif; ?>
							</div>
						
						</li>
					<?php $i++; endwhile; wp_reset_query(); ?>
					</ul>
				</section>
			</div>

			<!-- FORMULÁRIO DE CONTATO  -->
			<section class="contatoFranquias">
				<div class="container">
					<span>Entre em contato</span>
					<div class="formulario-contato">

						<?php echo do_shortcode('[contact-form-7 id="1003" title="Formulário rodapé página inicial"]'); ?>

					</div>
				</div>
			</section>
			
	</div>

	<?php include (TEMPLATEPATH . '/inc/mobile.php'); ?>
	
					
	<script type="text/javascript">
		$(window).bind('scroll', function () {
		   var alturaScroll = $(window).scrollTop()
		   if (alturaScroll > 500) {
		   		$(".navMenu").addClass("topoFixed");
		   }else{
		   		$(".navMenu").removeClass("topoFixed");
		   }
		});

		$('.menuMobile li').click(function(e){
			$('.menuMobile li .menuDrop').hide();
			$(this).children().slideDown();
		});
		$('.contentConteudo').click(function(e){
			$('.menuMobile li .menuDrop').slideUp();
		});
		$('.menuMobile li a').click(function(e){
			$('.menuMobile li .menuDrop').hide();
		});
	</script>

<?php get_footer(); ?>

<?php 

	$logoFraquias = $configuracao['opt-logo-restaurantes']['url'];

	$titulo = $configuracao['opt-frase-titulo-restaurantes'];

	

	$iconMapa = $configuracao['opt-icon-mapa-restaurantes']['url'];

	$formularioContato = $configuracao['opt-formulario-restaurantes'];



	// ENDEREÇO DOS RESTAURANTES 

	$endereco = $configuracao['opt-endereco-restaurantes'];

	$frase = $configuracao['opt-frase-restaurantes'];



	// BAR DO VICTOR PRAÇA DA ESPANHA 

	$enderecoE = $configuracao['opt-endereco-espanha'];

	$iconMapaE = $configuracao['opt-icon-mapa-espanha']['url'];



	//PETISCARIA

	$enderecoP = $configuracao['opt-endereco-Petiscaria'];	

	$iconMapaP = $configuracao['opt-icon-mapa-Petiscaria']['url'];



	// BISTRÔ

	$enderecoB = $configuracao['opt-endereco-bistro'];	

	$iconMapaB = $configuracao['opt-icon-mapa-bistro']['url'];



	//BAR DO VICTOR SÃO LOURENÇO 

	$enderecoS = $configuracao['opt-endereco'];

	$iconMapaS = $configuracao['opt-icon-mapa']['url'];



 // FUNÇÃO MAPA 



$curl = curl_init();


if ($endereco ) {
	
curl_setopt_array($curl, array(

   CURLOPT_RETURNTRANSFER => 1,

   CURLOPT_URL => 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBY7oA-HMU8HYmWtlQ1nJxWsWLTtpctYt0&address='.urlencode($endereco).'&sensor=false',

));

$geo = curl_exec($curl);



curl_close($curl);



$geo   = json_decode($geo, true);

}

///





// BAR DO VICTOR SÃO LOURENÇO 



$curl = curl_init();



curl_setopt_array($curl, array(

   CURLOPT_RETURNTRANSFER => 1,

   CURLOPT_URL => 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBY7oA-HMU8HYmWtlQ1nJxWsWLTtpctYt0&address='.urlencode($enderecoS).'&sensor=false',

));

$geoS 	= curl_exec($curl);



curl_close($curl);



$geoS   = json_decode($geoS, true);



///





// BISTRÔ

$curl = curl_init();



curl_setopt_array($curl, array(

   CURLOPT_RETURNTRANSFER => 1,

   CURLOPT_URL => 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBY7oA-HMU8HYmWtlQ1nJxWsWLTtpctYt0&address='.urlencode($enderecoB).'&sensor=false',

));

$geoB 	= curl_exec($curl);



curl_close($curl);



$geoB   = json_decode($geoB, true);



///





//PETISCARIA



$curl = curl_init();



curl_setopt_array($curl, array(

   CURLOPT_RETURNTRANSFER => 1,

   CURLOPT_URL => 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBY7oA-HMU8HYmWtlQ1nJxWsWLTtpctYt0&address='.urlencode($enderecoP).'&sensor=false',

));

$geoP 	= curl_exec($curl);



$geoP   = json_decode($geoP, true);



///



// BAR DO VICTOR PRAÇA DA ESPANHA 



$curl = curl_init();



curl_setopt_array($curl, array(

   CURLOPT_RETURNTRANSFER => 1,

   CURLOPT_URL => 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBY7oA-HMU8HYmWtlQ1nJxWsWLTtpctYt0&address='.urlencode($enderecoE).'&sensor=false',

));

$geoE 	= curl_exec($curl);



$geoE   = json_decode($geoE, true);





///


if ($endereco ) {
	if (isset($geo['results'])) {



		$latitude = $geo['results'][0]['geometry']['location']['lat'];

		$longitude = $geo['results'][0]['geometry']['location']['lng'];

	}
}



//BAR DO VICTOR SÃO LOURENÇO 

if (isset($geoS['results'])) {

	$latitudeS = $geoS['results'][0]['geometry']['location']['lat'];

	$longitudeS = $geoS['results'][0]['geometry']['location']['lng'];

}





//BAR DO VICTOR SÃO LOURENÇO 

if (isset($geoB['results'])) {

	$latitudeB = $geoB['results'][0]['geometry']['location']['lat'];

	$longitudeB = $geoB['results'][0]['geometry']['location']['lng'];

}



//PETISCARIA

if (isset($geoP['results'])) {

	$latitudeP = $geoP['results'][0]['geometry']['location']['lat'];

	$longitudeP = $geoP['results'][0]['geometry']['location']['lng'];

}



// BAR DO VICTOR PRAÇA DA ESPANHA 

if (isset($geoE['results'])) {

	$latitudeE = $geoE['results'][0]['geometry']['location']['lat'];

	$longitudeE = $geoE['results'][0]['geometry']['location']['lng'];

}



 ?>

<!-- FUNÇÃO PARA ADICIONAR ICONE NO MAPA -->

<script>



var map;

//POSICIONAMENTO DOS ESTABALECIMENTOS

//var pierDoVictor = {lat: <?php echo $latitude; ?>, lng: <?php echo $longitude; ?>};



//BAR DO VICTOR SÃO LOURENÇO 

var barVictorSaolourenco = {lat: <?php echo $latitudeS; ?>, lng: <?php echo $longitudeS; ?>};



//BAR DO VICTOR SÃO LOURENÇO 

var iconbarVictorSaolourenco = "<?php echo $iconMapaS; ?>";



// BISTRÔ

var bistro = {lat: <?php echo $latitudeB; ?>, lng: <?php echo $longitudeB; ?>};



// BISTRÔ

var iconBistro = "<?php echo $iconMapaB; ?>";



//PETISCARIA

var petiscaria = {lat: <?php echo $latitudeP; ?>, lng: <?php echo $longitudeP; ?>};



// BAR DO VICTOR PRAÇA DA ESPANHA 

var restauranteEspanha = {lat: <?php echo $latitudeE; ?>, lng: <?php echo $longitudeE; ?>};



// BISTRÔ

var iconiconMapaE = "<?php echo $iconMapaE; ?>";



//ICONES DOS ESTABELECIMENTOS

var iconePierDoVictor = "<?php echo $iconMapa; ?>";



//LINK PIN

//var endereco = "<?php echo 'https://www.google.com.br/maps/place/' . urldecode($geo['results'][0]['formatted_address']); ?>";



var enderecos = "<?php echo 'https://www.google.com.br/maps/place/' . urldecode($geoS['results'][0]['formatted_address']); ?>";



var enderecob = "<?php echo 'https://www.google.com.br/maps/place/' . urldecode($geoB['results'][0]['formatted_address']); ?>";



var enderecop = "<?php echo 'https://www.google.com.br/maps/place/' . urldecode($geoP['results'][0]['formatted_address']); ?>";



var enderecoe = "<?php echo 'https://www.google.com.br/maps/place/' . urldecode($geoE['results'][0]['formatted_address']); ?>";















// PETISCARIA

var iconptiscaria = "<?php echo $iconMapaP; ?>";



// BAR DO VICTOR PRAÇA DA ESPANHA 

var iconrestauranteEspanha = "<?php echo $iconMapaP; ?>";





function initMap() {



	//MAPA

	map = new google.maps.Map(document.getElementById('map'), {

		center: petiscaria,

		zoom: 12,

		disableDefaultUI: true,

		zoomControl: true,

		scrollwheel: false,

		draggable: false, 

		disableDoubleClickZoom: true,

		styles: [			

			{

				"featureType":"landscape",

				"elementType":"geometry.fill",

				"stylers":[{"color":"#ebebeb"}]

			},

			{

			    "featureType": "administrative",

			    "elementType": "labels",

			    "stylers": [

			      { "visibility": "off" }

			    ]

		  	},{

			    "featureType": "poi",

			    "elementType": "labels",

			    "stylers": [

			      { "visibility": "off" }

			    ]

		  	},{

			    "featureType": "water",

			    "elementType": "labels",

			    "stylers": [

			      { "visibility": "off" }

			    ]

		  	}

  		]

	});



	//MARKERS OU PINS DO MAPA

	// var markerPierDoVictor = new google.maps.Marker({

	//     position: pierDoVictor,

	//     icon: iconePierDoVictor,

	//     map: map

	// });



	// google.maps.event.addListener(markerPierDoVictor, 'click', function(e) {

	// 		 window.open(endereco);

	// 		// alert(endereco);

	// });





	//BAR DO VICTOR SÃO LOURENÇO 

	var markerbarSaoLourenco = new google.maps.Marker({

	    position: barVictorSaolourenco,

	    icon: iconbarVictorSaolourenco,

	    map: map

	});



	google.maps.event.addListener(markerbarSaoLourenco, 'click', function(e) {

		 window.open(enderecos);

	});



	// BISTRÔ

	var marKerBistro = new google.maps.Marker({

	    position: bistro,

	    icon: iconBistro,

	    map: map

	});



	google.maps.event.addListener(marKerBistro, 'click', function(e) {

		 window.open(enderecob);

	});



	// BISTRÔ

	var marKerpetiscaria = new google.maps.Marker({

	    position: petiscaria,

	    icon: iconptiscaria,

	    map: map

	});



	google.maps.event.addListener(marKerpetiscaria, 'click', function(e) {

		 window.open(enderecop);

	});



	// BAR DO VICTOR PRAÇA DA ESPANHA 

	var marKerptacadaespanha = new google.maps.Marker({

	    position: restauranteEspanha,

	    icon: iconiconMapaE,

	    map: map

	});



	google.maps.event.addListener(marKerptacadaespanha, 'click', function(e) {

		// location.href =enderecoe;

		 window.open(enderecoe);

	});



}



    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBY7oA-HMU8HYmWtlQ1nJxWsWLTtpctYt0&callback=initMap" async defer></script>