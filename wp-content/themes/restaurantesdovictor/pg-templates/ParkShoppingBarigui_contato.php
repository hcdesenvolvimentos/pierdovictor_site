<?php
session_start();
/**
 * Template Name: Contato - Park Shopping Barigui
 * Description: Página inicial do site  Restaurantes do Victor
 *
 * @package Restaurantes_do_Victor
 */
$formulario  = rwmb_meta('Restaurantesdovictor_contat_formulario'); 

$fotoContato = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoContato = $fotoContato[0];
global $configuracao;

get_header(); ?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modalContato" role="document">
    <div class="modal-content">
      
       <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal">&times;</button>

  		<div class="pg pg-contato modalCOntato">			
			
			
		
			<div class="formulario-contato trabalheConosco">
				<p class="subtitulo-contato text-center" id="nomeFormulario" >Trabalhe Conosco</p>		
				<?php echo do_shortcode('[contact-form-7 id="442" title="Formulário Trabalhe Conosco"]'); ?>
			</div>
			

			
			<div class="formulario-contato compras">
				<p class="subtitulo-contato text-center" id="nomeFormulario" >Compras</p>		
				<?php echo do_shortcode('[contact-form-7 id="443" title="Fomulário de compras"]'); ?>
			</div>
		

			<div class="formulario-contato adm">
				<p class="subtitulo-contato text-center" id="nomeFormulario" >Administrativo</p>		
				<?php echo do_shortcode('[contact-form-7 id="444" title="Formulário Administrativo"]'); ?>
			</div>

			<div class="formulario-contato sugestao">
				<p class="subtitulo-contato text-center" id="nomeFormulario" >Deixe sua sugestão</p>			
				<?php echo do_shortcode('[contact-form-7 id="442" title="Formulário Trabalhe Conosco"]'); ?>
			</div>
		
		</div>

      </div>
      
    </div>
  </div>
</div>
<div class="pg pg-contato">
	<div class="banner-contato" style="background:url(<?php  echo $fotoContato ?>)">
		<div class="nome-contato">
			<p>Contato</p>
			
		</div>
	</div>	
	<div class="row areaFomrulario">
		<div class="col-md-7">
			<p class="subtitulo-contato"><?php echo get_the_content() ?> <img src="<?php bloginfo('template_directory'); ?>/svg/aviao.svg" class="img-responsive imgContato" alt=""></p>
			<div class="formulariosIndividuais text-center">
				<ul>
					<li  data-toggle="modal" data-target="#myModal" class="tipoForm" data-tituloItem="Trabalhe Conosco" data-title="trabalheConosco">
						<div class="icon">
							<img src="<?php bloginfo('template_directory'); ?>/svg/trabalheconosco.svg" alt="" class="img-responsive">
						</div>

						<span>Trabalhe Conosco</span>
					</li>
					<li  data-toggle="modal" data-target="#myModal" class="tipoForm" data-tituloItem="Compras" data-title="compras">
						<div class="icon">
							<img src="<?php bloginfo('template_directory'); ?>/svg/compras.svg" alt="" class="img-responsive">
						</div>

						<span>Compras</span>
					</li>
					<li  data-toggle="modal" data-target="#myModal" class="tipoForm" data-tituloItem="Administrativo" data-title="adm">
						<div class="icon">
							<img src="<?php bloginfo('template_directory'); ?>/svg/adm.svg" alt="" class="img-responsive">
						</div>

						<span>Administrativo</span>
					</li>
					<li  data-toggle="modal" data-target="#myModal" class="tipoForm" data-tituloItem="Deixe sua sugestão" data-title="sugestao">
						<div class="icon">
							<img src="<?php bloginfo('template_directory'); ?>/svg/sugestao.svg" alt="" class="img-responsive">
						</div>

						<span>Deixe sua sugestão</span>
					</li>
				</ul>
			</div>
			<div class="formulario-contato">

				<?php echo do_shortcode($formulario); ?>

			</div>
		</div>
		<div class="col-md-5 text-center">
			
			<div class="info-contato-rodape">
				
				<img src="<?php echo  $configuracao['opt-logo']['url']  ?>" class="img-responsive" alt="">
				<!-- <p>Entre em contato </p> -->
				<span><?php echo $configuracao['p_nome'] ?></span>
				<?php 
					$horarioFormatados = explode(".", $horario);
					foreach ($horarioFormatados as $horarioFormatado) {
						$horarioFormatado = $horarioFormatado;
					
				 ?>
				<div class="horarios"><?php echo  $horarioFormatado  ?></div>
				<?php } ?>
				<small><?php echo  $configuracao['p_endereco']  ?></small>
				<small><strong><?php echo  $configuracao['p_telefone']  ?></strong></small>
				
			</div>

			
		</div>
	</div>
</div>		

<?php get_footer(); 
 include (TEMPLATEPATH . '/inc/scriptMapa.php');
?>
