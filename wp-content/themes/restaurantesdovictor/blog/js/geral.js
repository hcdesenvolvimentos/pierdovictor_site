$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	
	

	$(document).on('ready', function() {

		$(".center").slick({
			centerMode: true,
			centerPadding: '90px',
			slidesToShow: 1,
			speed: 2000,
			dots: true,
			responsive: [
			{
				breakpoint: 1333,
				settings: {
					centerMode: false,
				}
			},
			
			]
		});


	});

	//CARROSSEL DE DESTAQUE
	$("#carrosselCategoriaBlog").owlCarousel({
		items : 4,
        dots: true,
        loop: true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,
	    responsiveClass:true,
			    responsive:{
			         0:{
			            items:1,
			            
			        },
			        540:{
			            items:2,
			            
			        },
			        768:{
			            items:3,
			            
			        },
			        1190:{
			            items:4,
			            
			        }
			    }		  		
	    
	});
	
	
		
});