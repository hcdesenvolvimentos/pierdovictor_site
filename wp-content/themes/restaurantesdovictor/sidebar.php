<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Restaurantes_do_Victor
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div class="col-md-3">
	<div class="sidebar">
		<span class="titulo-sidebar-blog">categorias</span>
		<?php

			// CATEGORIA ATUAL
			$categoriaAtual = get_the_category();
			$categoriaAtual = $categoriaAtual[0]->cat_name;

			// LISTA DE CATEGORIAS
			$arrayCategorias = array();
			$categorias=get_categories($args);
			foreach($categorias as $categoria):
			$arrayCategorias[$categoria->cat_ID] = $categoria->name;
			$nomeCategoria = $arrayCategorias[$categoria->cat_ID];


		?>
		<a href="<?php echo get_category_link($categoria->cat_ID); ?>" class="linkSidebar"><?php echo $nomeCategoria ?></a>
		<?php endforeach; ?>

		<span class="titulo-sidebar-blog titulo-baixo">Mais vistos</span>

		<ul>
			<?php 
			 	$popularpost = new WP_Query( array( 'posts_per_page' => 5, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC' ) );
				
				while ( $popularpost->have_posts() ) : $popularpost->the_post();
				$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$foto = $foto[0];
				
			 ?>
			<li>
				<a href="<?php echo get_permalink(); ?>" class="linkpostagem">
					<img src="<?php echo $foto ?>" alt="<?php echo get_the_title() ?>" class="img-responsive">
					<p><?php echo get_the_title() ?></p>
					<span><?php the_time('j \F \ Y') ?></span>
				</a>
			</li>
			<?php  endwhile; wp_reset_query(); ?>
			</ul>
				
			<!-- MENU LATERAL -->
			<div class="menu-lateral2">
				<?php
$args = array(
    'excerpt_length' => 55,
    'post_html' => '<li>{thumb} {title}</li>'
);

wpp_get_mostpopular( $args );
?>
				<!-- <aside id="secondary" class="widget-area" role="complementary">
					<?php dynamic_sidebar( 'sidebar-1' ); ?>
				</aside> -->
			
			</div>
		
	</div>	
</div>