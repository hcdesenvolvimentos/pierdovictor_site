

<!-- FONTS -->
<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i|Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Vollkorn:400,400i,600,600i,700,700i,900,900i" rel="stylesheet">

<!-- CSS -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/cardapio/css/animate.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/jquery.fancybox.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/helpers/jquery.fancybox-buttons.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/helpers/jquery.fancybox-thumbs.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/cardapio/css/cardapio.css" />

<!-- JS -->
<script src="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/jquery.fancybox.pack.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/helpers/jquery.fancybox-buttons.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/helpers/jquery.fancybox-media.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/helpers/jquery.fancybox-thumbs.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/cardapio/js/cardapio.js"></script>