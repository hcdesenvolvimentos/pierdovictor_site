
<!-- SIDEBAR CARDÁPIO -->
<div class="contentCardapioSidebar">
	<div class="itemSidebar">
	<?php 

		// RECUPERANDO CATEGORIAS FILHAS
		$listaSubCategorias = get_categories($subCategoriaCardapio);

		
		
		// LISTANDO CATEGORIAS
		foreach ($listaSubCategorias as $listaSubCategorias):
			$listaSubCategorias = $listaSubCategorias;

			// VERIFICANDO SE EXITE
			$vefiricacao = array(
				'taxonomy'     => 'categoriaCardapio',
				'child_of'     => 0,
				'parent'       => $listaSubCategorias->cat_ID,
				'orderby'      => 'name',
				'pad_counts'   => 0,
				'hierarchical' => 1,
				'title_li'     => '',
				'hide_empty'   => 0
			);	

			$vefiricacao = get_categories($vefiricacao);
			if ($vefiricacao):

	?>
		<a href="#<?php echo $listaSubCategorias->slug ?>" class="scroll">
			<h2><?php echo $listaSubCategorias->name ?></h2>
		</a>
	<?php endif;endforeach; ?>
	</div>
</div>
