<!-- CARDÁPIO -->
<section class="cardapioInicial" style="background: url(<?php echo get_template_directory_uri() ?>/cardapio/img/fotoHome2.jpg);">

	<button class="fecharCardapio" id="fecharInicial">X</button>
	<div class="sidebar">
		<!-- LOGO -->
		<img src="<?php echo get_template_directory_uri() ?>/cardapio/img/logoCardapio.png" alt="">
		
		<!-- TÍTULO -->
		<div class="cardapioInicialTitulo">
			<p>Selecione</p>
			<span>uma das opções no menu abaixo:</span>
		</div>

		
		<?php 
			// RECUPERANDO CATEGORIAS
			$categoriaCardapio = array(
				'taxonomy'     => 'categoriaCardapio',
				'child_of'     => 0,
				'parent'       => 0,
				'orderby'      => 'name',
				'pad_counts'   => 0,
				'hierarchical' => 1,
				'title_li'     => '',
				'hide_empty'   => 0
			);
			$listaCategorias = get_categories($categoriaCardapio);


			if ($categoriaCardapio ):


					
		?>	
		<!-- SIDEBAR -->
		<aside>
			<img src="<?php echo get_template_directory_uri() ?>/cardapio/img/logoCardapio.png" alt="" class="logoSidebar">
			<nav>
				<?php foreach ($listaCategorias  as $listaCategorias):

					// RECUPERANDO CATEGORIAS FILHAS/PASSAR PARAMETRO CAT_ID
					$subCategoriaCardapio = array(
						'taxonomy'     => 'categoriaCardapio',
						'child_of'     => 0,
						'parent'       => $listaCategorias->cat_ID,
						'orderby'      => 'name',
						'pad_counts'   => 0,
						'hierarchical' => 1,
						'title_li'     => '',
						'hide_empty'   => 0
					);	
						$subCategoriaCardapio = get_categories($subCategoriaCardapio);

						if ($subCategoriaCardapio):
							
						
						$listaCategoria = $listaCategorias;
						
						$categoriaAtivaImg = z_taxonomy_image_url($listaCategoria->term_id);
				?>
				<h3 data-slug="<?php echo $listaCategoria->slug ?>"><?php echo $listaCategoria->name ?></h3>
				<?php endif;endforeach; ?>
			</nav>
		</aside>
		<?php endif; ?>
	</div>

</section>