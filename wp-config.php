<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'projetos_pierdovictor_site');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'O&t3olzo{+466NLWC!<RVi%]{5@MA#>FIB1D%H;Gai9dS6cWLd8(RTs?t#)_z/v*');
define('SECURE_AUTH_KEY',  'x3D),P3g6w%eSuQGvzw5p36jqz^pppIeE9$r=;p!FVlT1a(GCkDF}7+lcupHR31v');
define('LOGGED_IN_KEY',    'ctLIrM+pUnO{.8!i`G}!TBN gBuSEn.|;d$UyENrvWFYPA&2@j(7O!Ld]cA~BHtK');
define('NONCE_KEY',        '[7mk&tR>}ndyQBd5@L$ O.x:EDO,,!c+bDb `]{x2I4tTyK>@f%B6GP+;A$OReGh');
define('AUTH_SALT',        'zZ<Ta!G~3Wf(G3VTA-&G@zGLuM71E&jjBCt[uk ~&vo~a]<}@V. *{wE8ps|Pa((');
define('SECURE_AUTH_SALT', 'LBffEAtGp@<8Oj7Qi!uSE:V ;N^jNiW^%3k,Ixwqm<&hcup&#6Gwq)!5P/6-U*Vw');
define('LOGGED_IN_SALT',   'PdB,c8mE7.[mSR{pcy:Vo$YL%W0A4@PU8l4PMP)n3X+JcA%bj!|`~.QD#Uau[.vj');
define('NONCE_SALT',       'zJ&~*4~1|2V+wOt1!+qWNnNrzSp1vJ.@Gr}7[Se+*Z33s7pZ@P4;&oG=0rt2~&(]');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'pv_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
